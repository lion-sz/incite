import pathlib
import json
from typing import Optional, Union, List

from incite.citation import Citation
from incite.styles import *


def dump_citations(
    citation: Union[Citation, List[Citation]],
    path: Union[None, pathlib.Path, str] = None,
) -> Optional[str]:
    """
    Dump a citation or list of citations to file.

    Args:
        citation (:class:`Citation` or List[:class:`Citation`]): Either a single citation or a list of citations,
            which should be written to the file.
        path (pathlib.Path or str, optional): If provided, the results will be written to the file.
            Otherwise, I return the string.

    Returns:
        str or None: A string if no filename was provided, otherwise the results are written to a file.
    """
    # First check the file types.
    if isinstance(citation, Citation):
        citation = [citation]
    else:
        if type(citation) is not list:
            raise ValueError(
                f"Citations must be either a citation or list of citations, not {type(citation)}."
            )
        if not all([isinstance(cit, Citation) for cit in citation]):
            raise ValueError("All elements must be a citation.")
    if path is not None:
        if type(path) is str:
            path = pathlib.Path(path)
        elif type(path) is not pathlib.Path:
            raise ValueError(
                f"filename must be either string or pathlib Path, not {type(path)}."
            )
    # first collect all citations as tuples
    cit_list = [cit.to_dict() for cit in citation]
    if path:
        json.dump(cit_list, path)
    else:
        return json.dumps(cit_list)


def load_citations(
    path: Union[None, pathlib.Path, str] = None, data: Optional[str] = None
) -> List[Citation]:
    """
    Load a list of citations from either a file or the data argument.
    Exactly one of the two must be provided.

    Args:
        path (pathlib.Path or str, optional): The file from which to load the citations.
        data (str, optional): The string, which to load.

    Returns:
        List[:class:`Citation`]: The citations stored.
    """
    if path is None and data is None:
        raise ValueError("One of path or data must be specified.")
    if path is not None and data is not None:
        raise ValueError("Only one of path and data must be specified.")
    # first the code to read in from the path
    if path is not None:
        if type(path) is str:
            path = pathlib.Path(path)
        else:
            raise ValueError(f"path must be of type pathlib.Path, not {type(path)}")
        if not path.exists():
            raise ValueError(f"File {str(path)} does not exist.")
        with path.open("r") as jfile:
            cit_data = json.load(jfile)
    if data is not None:
        if type(data) is not str:
            raise ValueError(f"data must be of type str, not {type(path)}")
        cit_data = json.loads(data)
    # now parse the citation data back into a list of citations
    # First I need to check if the loaded object is a dictionary,
    # if this is the case, convert it to a list.
    if type(cit_data) is dict:
        cit_data = [cit_data]
    citations = []
    for cit in cit_data:
        citations.append(get_style(cit["citation_style"]).from_dict(cit))
    return citations
