from typing import Optional


def extract_numeral(data: str) -> Optional[int]:
    """
    This function extracts a number from a string.

    :param data: The word from which a number should be extracted.
    :return: The found Number. None if none was found.
    """
    tmp = "".join(filter(lambda x: x.isdigit() or x == "-", data))
    if len(tmp) == 0 or len("".join(filter(str.isdigit, tmp))) == 0:
        return None
    return int(tmp) if int(tmp) > 0 else None
