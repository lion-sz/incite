"""
Citation Interface
------------------

This module contains a class from which all citations inherit from.
It specifies the parts which all citation objects have in common, as well as the
shared methods the citations have.
"""

from __future__ import annotations
from typing import List, Union, Dict


class Citation:
    """
    Each citation has several class attributes, which specify which citation style the
    class belongs to, a list of elements that the style has and which types these elements have.
    Furthermore, each citation contains the context in which it was found.

    A citation can be casted to string.
    They can also be dumped to file or string and loaded again through the classmethods
    :func:`~Citation.dump` and :func:`~Citation.load`.

    Citations of the same style can be compared for both equality and whether a citation contains another.
    The elements of a citation can be accessed and set through indexing.
    """

    citation_style: str
    context: List[str]
    elements: List[str]
    """The elements of the citation style."""
    types: List[str]
    """The types of the citation elements."""

    @classmethod
    def blank(cls, context):
        return cls(context)

    @classmethod
    def from_tuple(cls, data):
        """
        Creates an new Citation from a tuple of citation
        parts. The context and text are left blank.

        Args:
            data: A tuple of the citation data with exactly 5 elements
        Returns:
            A new citation, or None if the tuple is bad.
        """
        raise NotImplementedError(
            "'from_tuple' method is not implemented for the base class!"
        )

    @classmethod
    def from_dict(cls, citation_data: Dict[str, Union[str, int]]) -> Citation:
        """
        Create a new citation from a citation dictionary.
        I check that I indeed have the correct citation class and that all elements
        were understood. If one of these conditions is violated, print a warning.

        Args:
            citation_data (Dict[str, Union[int, str]]): The citation dictionary, as dumped by :class:`Citation.to_dict`.
        Returns:
            Citation: A new citation.
        """
        tmp = cls(citation_data["context"].split(" "))
        del citation_data["context"]
        if citation_data["citation_style"] != cls.citation_style:
            print(
                f"Warning: The dumped citations style differs from the called style: {citation_data['citation_style']} and {cls.citation_style}"
            )
        del citation_data["citation_style"]
        for elem in tmp.elements:
            if elem in citation_data:
                tmp[elem] = citation_data[elem]
                del citation_data[elem]
        if len(citation_data) > 0:
            print(f"Warning: citation keys left: {[str(k) for k in citation_data]}")
        return tmp

    def to_dict(self) -> Dict[str, Union[int, str]]:
        """
        Dump the citation to a json friendly dictionary.
        Values not set (None) will not be present in the dictionary.

        Returns:
            Dict[str, Union[int, str]]: The citation as a dictionary.
        """
        res = {"citation_style": self.citation_style, "context": " ".join(self.context)}
        for elem in self.elements:
            if self[elem] is not None:
                res[elem] = self[elem]
        return res

    def __init__(self, context: List[str], **kwargs):
        """
        To create a new instance of a citation, you requite the context in which the citation was found.
        The parts of the citation are then passed as keyword arguments.
        """
        self.context = context
        for elem in self.elements:
            if elem in kwargs:
                self[elem] = kwargs[elem]

    def set_value(self, key: str, value: Union[int, str], overwrite: bool = False):
        """
        This function sets a citation element to the given value.

        Args:
            key (str): The parameter value
            value (int or str): The value
            overwrite (bool (default False)): Whether to overwrite an existing attribute.
                If False, setting an existing attribute silently does nothing.
        """
        if key not in self.elements:
            raise ValueError(
                f"Key {key} not supported for style {self.citation_style}."
            )
        if getattr(self, key) is not None and not overwrite:
            return
        setattr(self, key, value)
        return

    def __getitem__(self, item):
        return getattr(self, item)

    def __setitem__(self, key, value):
        return self.set_value(key, value, overwrite=False)

    def __eq__(self, other) -> bool:
        """
        Two citations can only be equal, if both citations are of the same class.
        To check for equality I go through all elements defined in the
        elements list.
        """
        if type(other) is not type(self):
            return False
        for key in self.elements:
            if self[key] != other[key]:
                return False
        return True

    def __contains__(self, item) -> bool:
        """
        A citation contains another citation of the same class,
        if the second is to the same place as the first, but finer.

        Therefore, I check that for each not None element of the first,
        the second has the same.
        Thus, I return False if either left has something, while right has nothing,
        or the values are unequal.
        """
        if type(item) is not type(self):
            return False
        for key in self.elements:
            if self[key] is not None and item[key] is not None:
                return False
            if self[key] != item[key]:
                return False
        return True

    def __str__(self) -> str:
        raise NotImplementedError("Cannot cast baseclass to string.")
