from __future__ import annotations
from typing import Optional, List, Tuple
import copy

from incite.citation import Citation
from incite._utils import extract_numeral


class GerKeywords(Citation):
    """
    German keyword based citation.
    """

    citation_style = "GerKeywords"
    elements = ["paragraph", "absatz", "satz", "buchstabe", "gesetz"]
    types = ["int", "int", "int", "str"]

    paragraph: Optional[int]
    absatz: Optional[int]
    satz: Optional[int]
    buchstabe: Optional[str]
    gesetz: Optional[str]

    def __init__(
        self,
        context: List[str],
        paragraph: Optional[int] = None,
        absatz: Optional[int] = None,
        satz: Optional[int] = None,
        buchstabe: Optional[str] = None,
        gesetz: Optional[str] = None,
    ):
        super().__init__(context)
        self.paragraph = paragraph
        self.gesetz = gesetz
        self.absatz = absatz
        self.satz = satz
        self.buchstabe = buchstabe

    def is_valid(self) -> bool:
        if self.paragraph is None or self.gesetz is None:
            return False
        return True

    def __str__(self) -> str:
        if not self.is_valid():
            return "Invalid Citation"
        res = f"Artikel {self.paragraph}"
        if self.absatz is not None:
            res += f" Absatz {self.absatz}"
        if self.satz is not None:
            res += f" Satz {self.satz}"
        if self.buchstabe is not None:
            res += f" Buchstabe {self.buchstabe}"
        return f"{res} {self.gesetz}"


class GerKeywordsStyle:
    """
    The style to detect german citations based on keywords.
    """

    citation_keys = {
        "paragraph": {"art", "art.", "article", "§"},
        "absatz": {"absatz", "abs", "abs."},
        "satz": {"satz", "s", "s."},
        "buchstabe": {"buchstabe", "lit", "lit."},
    }
    """
    These are the keywords used.
    """
    fillers = {"der"}
    max_irrelevant = 1
    """
    This parameter specifies how many irrelevant words a citation can have before I stop parsing.
    0 means that I stop when I encounter even one.
    """
    join_words = {"und"}
    """
    These words are used to join two citations, e.g. Art 5 und 7 DSGVO.
    If I encounter one of these words I allow the following to be any citation element,
    while a ',' is only allowed for a second value of the same element.
    """

    def spot_citation(self, context: List[str]) -> bool:
        """
        Spot whether there is a citation in the context position.
        I simply check if the last word in the context list is in the paragraph keywords.

        Args:
            context (List[str]): The list of words. I keep the option for this to the entire sentence
                leading up to the final word for potential later extraction strategies.
        Returns:
            bool: Whether there is a citation.
        """
        return context[-1] in self.citation_keys["paragraph"]

    def extract_citation(self, context: List[str]) -> Tuple[List[Citation], int]:
        """
        This function extracts a citation from a possible citation from a list of words.
        To reconstruct the sentence from the citations,
        I need to return the position in the string, that the parser stopped at.

        Args:
            context (List[str]): The context which to look through for a citation.

        Returns:
            Tuple[List[Citation], int]: A Tuple of a list of the valid citations
                and the number of words that belong to these citations.
        """
        citation_text = [
            context[0]
        ]  # This list stores the elements that belong to the citation.
        # Now I need to go through this possible citation
        # and try to extract information from it.
        # first the article, which I know to be in a fixed position.
        cits = [GerKeywords.blank(context)]
        cit_pos = 1
        cit_elem_pos = 0
        irrelevant = 0
        cit_struct_pos_before_join: Optional[int] = None
        while cit_pos < len(context) and irrelevant < self.max_irrelevant:
            word = context[cit_pos]
            # first look for a filler word and simply continue.
            if word in self.fillers:
                cit_pos += 1
                citation_text.append(word)
                continue
            # now check if there is a join word and I need to start a new citation.
            if word in self.join_words:
                # Write the text so far to the citation and create a new one.
                # There are two possible cases. Either a new citation begins with a new article
                # (for this I need to reset cit_elem_pos to zero, so that I can catch this),
                # or I continue from the current level (Art 1 Absatz 2 und 3 DSGVO).
                # To work with this I need a variable that saves this information.
                cits[-1]["text"] = " ".join(citation_text)
                citation_text: List[str] = []
                cits.append(Citation.blank(context))
                cit_struct_pos_before_join = (
                    cit_elem_pos  # I can reset this once I find a new reference.
                )
                cit_elem_pos = 0
                cit_pos += 1
                continue
            # first check If I find a new key.
            found_new = False
            for j in range(len(GerKeywords.elements) - cit_elem_pos - 1):
                if (
                    word.lower()
                    in self.citation_keys[GerKeywords.elements[cit_elem_pos + j]]
                ):
                    # I've found a new structure part. Increase the structure and reset the value.
                    citation_text.append(word)
                    cit_elem_pos += j
                    cit_pos += 1
                    cit_struct_pos_before_join = None
                    found_new = True
                    break
            if found_new:
                continue
            # look for a value
            # first I look for numeric values.
            if GerKeywords.elements[cit_elem_pos][1]:
                tmp_val = extract_numeral(word)
            else:
                # look for a single letter.
                tmp_val = "".join(filter(str.isalpha, word))
                if len(tmp_val) != 1:
                    tmp_val = None
            # Now I need to set the value and check for comma separated continuations.
            # I find these by having two valid values, while the previous word contained a ','.
            # The check for a valid value is done by the set_value function
            if tmp_val is not None:
                # first check If I need to use the last position. If I do, I don't need to check for commas.
                if cit_struct_pos_before_join is not None:
                    cits[-1][GerKeywords.elements[cit_elem_pos]] = tmp_val
                else:
                    citation_text.append(word)
                    # check for comma separated stuff.
                    if not cits[-1].set_value(
                        GerKeywords.elements[cit_elem_pos], tmp_val
                    ):
                        if "," in citation_text[-1]:
                            # this is a comma joined citation. Otherwise skip the new value.
                            new_cit = copy.deepcopy(cits[-1])
                            new_cit.set_value(
                                GerKeywords.elements[cit_elem_pos],
                                tmp_val,
                                overwrite=True,
                            )
                            cits[-1].text = " ".join(
                                citation_text
                            )  # this text still contains the comma...
                            citation_text.pop()
                            citation_text.append(word)
                            cits.append(new_cit)
            else:
                irrelevant += 1
            cit_pos += 1
        # write the gesetz and to all citations.
        cits[-1].text = " ".join(citation_text)
        end_position = cit_pos - irrelevant
        if end_position >= len(context):
            print("Warning: end position after last word in possible, resetting")
            end_position = len(context) - 1
        for cit in cits:
            cit["gesetz"] = context[end_position]
            # cit["text"] = cit["text"] + " " + cit["gesetz"]
        return [cit for cit in cits if cit.is_valid()], end_position + 1
