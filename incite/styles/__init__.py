from incite.styles.keyword import GerKeywords
from incite.styles.keyword import GerKeywordsStyle

_styles = {"GerKeywords": (GerKeywords, GerKeywordsStyle)}


def list_styles():
    """
    Returns:
        List[str]: This method lists the available styles.
    """
    return [k for k, v in _styles.items()]


def get_style(style: str):
    return _styles[style][0]


def _get_style_processor(style: str):
    return _styles[style][1]
