from incite.citation import Citation
import bs4
from typing import Callable, Optional, List, Union
from incite.styles import _get_style_processor


class CitationParser:
    """
    This parser is the main interface to this package.
    It provides several methods for standardizing and extracting citations,
    both from text and from html elements.

    To find citations, you need to register a citation style first.
    This is done by calling the :func:`CitationParser.register_style` method with the name of the style.
    For a list of all available styles, look at :func:`incite.styles.list_styles`.
    """

    _styles = []
    _tag_creation_callable = Callable[[Citation], bs4.element.Tag]

    def __init__(self, style: Union[None, str, List[str]] = "GerKeywords"):
        """
        When instantiating a Parser you can directly specify one or multiple styles to use.
        By default, the German Keyword style is used, but you can specify no style by setting None.

        Args:
            style (str, List[str], None (default "GerKeywords")): The style to use.
                This can either be a style name, or a list of names. If None, no style is set.
        """
        if style is None:
            return
        if type(style) is str:
            self._styles = [_get_style_processor(style)()]
        if type(style) is list and all([type(s) is str for s in style]):
            self._styles = [_get_style_processor(s)() for s in style]

    def register_style(self, style: str, first: bool = False):
        """
        Register a new style which to check.

        Args:
            style (str): The style which to register.
                To get a list of available styles, call :func:`incite.styles.list_styles`.
            first (bool (default False)): Whether to insert the style in the first position.
                If false (default), the style is inserted at the end.
        """
        # I need to check the style here
        tmp = _get_style_processor(style)()
        if not first:
            self._styles.append(tmp)
        else:
            self._styles.insert(0, tmp)

    def set_html_style(
        self,
        tag: str = "span",
        tag_style: Optional[str] = None,
        tag_class: Optional[str] = None,
    ) -> Callable[[Citation], bs4.element.Tag]:
        """
        This function is used to create a callable for formatting a citation into a html
        element.

        Args:
            tag (str, default="span"): The tag in which the citation should be enclosed.
            tag_style (str, optional): The css style attribute of the tag.
            tag_class (str, optional): The class attribute of the tag.

        Returns:
            Callable[[Citation], bs4.element.Tag]: A function which is called with a citation and
                formats this into a new html element.
        """
        tag = bs4.element.Tag(name=tag)
        if tag_class:
            tag["class"] = tag_class
        if tag_style:
            tag["style"] = tag_style
        return lambda cit: tag.append(str(cit))

    def _process(self, text: str) -> List[Union[str, Citation]]:
        """
        I process

        Args:
            text (str): The text from which citations should be extracted.

        Returns:
            (List[Union[str, Citation]]): The processed text
        """
        words = [w.strip() for w in text.split() if w.strip() != ""]
        processed: List[Union[str, Citation]] = []
        # now iterate through the words. I need the position, therefore with i.
        end_pos = 0  # The end of the previous citation.
        for i in range(len(words)):
            if i < end_pos:
                # this word was processed as part of a citation. Skip it.
                continue
            # append the word - if there is a citation, remove it later
            processed.append(words[i])
            # check through all styles.
            for style in self._styles:
                if style.spot_citation([words[i].lower()]):
                    # there is a citation here. Try to process it, if this succeeds, break.
                    cits, cit_len = style.extract_citation(
                        [words[i + j] for j in range(min(50, len(words) - i))]
                    )
                    if cits:
                        processed.pop()
                        for cit in cits:
                            processed.append(cit)
                        end_pos = i + cit_len
                        break
        return processed

    def replace_html(self, soup: bs4.element.Tag, **kwargs) -> None:
        """
        Correctly parsing citations from html is actually quite complicated.
        I can't simply go through the text of the element, since there might be nested structures.
        If a tags contain other child tags, I need to keep the position of these tags relative to the text
        of the parent tag. Otherwise I might end up shuffling text around.

        To prevent this, I check if the element has any children.
        If the element has, I iterate through all children, search for citations in the text around all elements
        and recursively apply this function to all children with more than 3 words of text
        distributed across any of their children.
        Elements with less than three words of text are simply removed and their text added to the previous text.

        An option to keep (or reconstruct) these small tags should be implemented later.

        Args:
            soup (bs4.element.Tag), The bs4 element which in which the method should search for citations.

        Returns:
            None: No return, since the soup argument is changed.
        """
        if self._tag_creation_callable is None:
            print("setting default html style, since none was provided earlier.")
            self.set_html_style()
        text = soup.text.strip()
        if len(text) == 0:
            return None
        processed_full = self._process(text)
        cits_total = len(list(filter(lambda x: type(x) is Citation, processed_full)))
        if cits_total == 0:
            # There are no citations anywhere in this tag, I can return.
            return None
        content: List[Union[str, bs4.element.Tag]] = []
        prev_text = ""
        for child in soup.children:
            if type(child) is bs4.element.Tag:
                if len(child.text.split()) >= 3:
                    self.replace_html(child)
                    if prev_text:
                        content.append(prev_text)
                        prev_text = ""
                    content.append(child)
                else:
                    prev_text += child.text
            else:
                prev_text += child
        if prev_text:
            content.append(prev_text)
        # Now clear the tag and rebuild it form the contents.
        soup.clear()
        for elem in content:
            if type(elem) is bs4.element.Tag:
                # this is an element, just append it.
                soup.append(elem)
            else:
                processed = self._process(elem)
                tmp: List[str] = []
                for part in processed:
                    if type(part) is str:
                        tmp.append(part)
                    elif type(part) is Citation:
                        if tmp:
                            soup.append(" ".join(tmp))
                        soup.append(self._tag_creation_callable(part))
                    else:
                        print("False type found")
                if tmp:
                    soup.append(" ".join(tmp))

    def replace(self, text: str) -> str:
        """
        This function replaces all citations in the input string with the standardized
        citation format.

        :param text: The input text that should be processed.
        :type text: str
        :return: The input text but with the standardized citations.
        :rtype: str
        """
        processed = self._process(text)
        res = []
        for elem in processed:
            if isinstance(elem, str):
                res.append(elem)
            else:
                res.append(str(elem))
        return " ".join(res)

    def extract(self, text: str) -> List[Citation]:
        """
        This function extracts and returns a list of citations.

        :param text: The text on which to work.
        :return: A list of the extracted citations.
        :rtype: List[Citation]
        """
        processed = self._process(text)
        return list(filter(lambda x: isinstance(x, Citation), processed))
