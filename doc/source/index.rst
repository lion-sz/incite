.. InCite documentation master file, created by
   sphinx-quickstart on Sun Sep 26 18:16:59 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to InCite's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   API Specification <api.rst>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
