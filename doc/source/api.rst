API Specification
=================


Parser
######

.. automodule:: incite.parser
    :members:

Citations and Citation Styles
#############################

.. automodule:: incite.citation
    :members:

.. automodule:: incite.styles
    :members: list_styles
