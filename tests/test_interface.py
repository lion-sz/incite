"""
Testing that the general interface functions work.
"""

import os
import sys
import json

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))

import incite


def test_replace_ger_keywords():
    parser = incite.CitationParser("GerKeywords")
    with open("test_data/replace_ger_keywords.json", "r") as jfile:
        dat = json.load(jfile)
    for row in dat:
        assert parser.replace(row["in"]) == row["out"]
