"""
These tests depend on the string and equality functions of the citation.
"""

import os
import sys

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))

import incite
import incite.styles as styles


ger_citation_class = styles.get_style("GerKeywords")

ger_cit_1 = ger_citation_class(["Art.", "6", "DSGVO"], 6, None, None, None, "DSGVO")
ger_cit_1_dump = '{"citation_style": "GerKeywords", "context": "Art. 6 DSGVO", "paragraph": 6, "gesetz": "DSGVO"}'


def test_dump_citation_str():
    assert (
        incite.dump_citations(ger_cit_1) == f"[{ger_cit_1_dump}]"
    ), "Dumping a single citation"
    assert incite.dump_citations(ger_cit_1) == incite.dump_citations(
        [ger_cit_1]
    ), "Dumping single citation list"
    assert (
        incite.dump_citations([ger_cit_1, ger_cit_1])
        == f"[{ger_cit_1_dump}, {ger_cit_1_dump}]"
    ), "Dumping several citations"


def test_load_citations_str():
    assert incite.load_citations(data=ger_cit_1_dump) == [
        ger_cit_1
    ], "Loading single citation"
    assert incite.load_citations(data=f"[{ger_cit_1_dump}]") == [
        ger_cit_1
    ], "Loading single citation List"
